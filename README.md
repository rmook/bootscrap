##SEF Web Development Application Seed and Guidelines

Things move fast up here, so assume that the project you are working on will be handed off to other developers within in the SEF lab, without the chance to brief them on the details. To make this process smooth, please:

+ Share Code, and solicit feedback.

+ Ask Questions!

+ Use version control. (Bitbucket is the Git based source code management service we prefer because it allows private repos to be created free of charge.)

+ As projects proceed, please test on multiple browsers, including. IE 8 and above, Chrome, and Firefox.

+ Lastly, learn .NET (This is ultimately the language that MOST projects going into production will be developed in. Also, when our time is through in the SEF lab, .NET is the language we will be developing in.)

##Seed Application Dependencies
* AngularJS

  + angular.min.js

  + angular-route.min.js

* Bootstrap (with BootstrapJS and Enterprise-custom theme)

  + Font Awesome and Glyphicons

  + bootstrap.min.css

  + bootstrap.min.js

  + bootstrap-theme.min.css

* jQuery and jQuery UI

  + jquery-1.10.*.min.js

  + jquery-ui.min.js

##Tips

###Things we like:
+ HTML5/CSS3
+ MVC Architecture: **Angular.js**
+ Front-end Frameworks: **Bootstrap**, **jQuery**
+ Plugins (Any plugin that will save us time. Don’t recreate the wheel.)
+ Modular Coding (Code in chunks)
+ Comments (Comments can save a developer a lot of time)
+ Source Control: **BitBucket** (Git Version Control)
+ 4 Space Indent
+ CSS links located in the header
+ JS scripts located right before the ending body tag.


###Things we DON’T like:
+ Other frameworks (Bourbon, Neat, Foundation – sorry folks)
+ Inline Styles (i.e. <div style=”color:green;”>Text</div>)
+ Style Tags (i.e. <font> <u> <b> <i>)
+ No comments or unclear/unhelpful comments
+ Scripts in the header
+ Sloppy Code
