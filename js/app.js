﻿/**
 * Main AngularJS Web Application
 */
var app = angular.module('WebApp', [
  'ngRoute',
  'panelControllers'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      // Home
      .when("/", {
          templateUrl: "partials/home.html",
          controller: 'BodyController'
      })
      // Pages
      .when("/page", {
          templateUrl: "partials/another_page.html",
          controller: 'BodyController'
      })
      // Member
      .when("/member", {
          templateUrl: "partials/member.html",
          controller: 'BodyController'
      })
      // else 404
      .otherwise({
          redirectTo: "/",

      });
}]);